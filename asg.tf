# provider
provider "aws" {
  region = "us-east-1"
}
# define ami
data "aws_ami" "amazon-linux-2" {
  most_recent = true
  owners           = ["amazon"]
  filter {
   name   = "owner-alias"
   values = ["amazon"]
 }
  filter {
   name   = "name"
   values = ["amzn2-ami-hvm*"]
 }
}
# define autoscaling launch configuration
resource "aws_launch_configuration" "custom_launch_conf" {
  image_id      = data.aws_ami.amazon-linux-2.id
  instance_type = "t2.micro"
  name = "demo-instance"

  lifecycle {
    create_before_destroy = true
  }
}
# define autoscaling group
resource "aws_autoscaling_group" "custom_asg" {
  name                      = "test-asg"
  max_size                  = 3
  min_size                  = 1
  health_check_grace_period = 300
  desired_capacity          = 1
  force_delete              = true
  launch_configuration      = aws_launch_configuration.custom_launch_conf.name
  availability_zones = ["us-east-1a"]
}
# define autoscaling group policy
resource "aws_autoscaling_policy" "custom_asg_policy" {
  name                   = "demo_asg_policy"
  scaling_adjustment     = 4
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.custom_asg.name
}
# define cloud watch monitoring
resource "aws_cloudwatch_metric_alarm" "custom_cloudwatch" {
  alarm_name          = "demo_cloudwatch_alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "80"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.custom_asg.name
  }

  alarm_description = "This metric monitors ec2 cpu utilization"
  alarm_actions     = [aws_autoscaling_policy.custom_asg_policy.arn]
}
resource "aws_lb" "alb_demo" {
  internal           = false
  load_balancer_type = "application"
  enable_deletion_protection = true
     tags = {
    Environment = "dev"
  }
}

resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_lb.alb_demo.arn
  port              = "443"
  protocol          = "HTTPS"
  
  default_action {
    type             = "forward"
  }
}
resource "aws_lb_target_group" "test_group" {
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.main.id
}
resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_lb_target_group_attachment" "test" {
  target_group_arn = aws_lb_target_group.test_group.arn
  target_id = aws_lb_listener.alb_listener.id
  port             = 80
}